module Lib
    ( someFunc
    ) where

import qualified Graphics.Gloss as G
import qualified Graphics.Gloss.Interface.IO.Game as G
import qualified Graphics.Gloss.Data.Point.Arithmetic as G
import qualified Graphics.Gloss.Data.Vector as G
import qualified System.Random.MWC as R
import Control.Monad.Primitive ( PrimMonad
                               , PrimState)
import Control.Monad (replicateM)
import Control.Arrow
import System.Exit (die)
import Data.Foldable (minimumBy, maximumBy, any, all)
import Data.List (sortBy)
import Data.Function (on)

default (Int, Float)

class Coord2 a where
  x :: a -> Float
  y :: a -> Float

instance Coord2 (Float, Float) where
  x (v, _) = v
  y (_, v) = v

class ToPoint a where
  toPoint :: a -> G.Point
  fromPoint :: G.Point -> a

class Object a where
  position     :: a -> G.Point
  radius       :: a -> Float
  mass         :: a -> Float
  velocity     :: a -> G.Point
  acceleration :: a -> G.Point
#if 0
  setPos       :: G.Point -> a -> a
#endif

data Pos where
  Pos :: { pX :: !Float
         , pY :: !Float
         } -> Pos
  deriving (Show, Eq)

instance Coord2 Pos where
  x (Pos v _) = v
  y (Pos _ v) = v

instance ToPoint Pos where
  toPoint (Pos x y) = (x, y)
  fromPoint = uncurry Pos

instance R.Variate Pos where
  uniform g = do
    pX <- R.uniform g
    pY <- R.uniform g
    return Pos{..}

  uniformR (Pos x y, Pos x' y') g = do
    pX <- R.uniformR (x, x') g
    pY <- R.uniformR (y, y') g
    return Pos{..}

data Acc where
  Acc :: { aX :: !Float
         , aY :: !Float
         } -> Acc
  deriving (Show, Eq)

instance Coord2 Acc where
  x (Acc v _) = v
  y (Acc _ v) = v

instance ToPoint Acc where
  toPoint (Acc x y) = (x, y)
  fromPoint = uncurry Acc

instance R.Variate Acc where
  uniform g = do
    aX <- R.uniform g
    aY <- R.uniform g
    return Acc{..}

  uniformR (Acc x y, Acc x' y') g = do
    aX <- R.uniformR (x, x') g
    aY <- R.uniformR (y, y') g
    return Acc{..}

data Speed where
  Speed :: { vX :: Float
           , vY :: Float
           } -> Speed
  deriving (Show, Eq)

instance Coord2 Speed where
  x (Speed v _) = v
  y (Speed _ v) = v

instance ToPoint Speed where
  toPoint (Speed x y) = (x, y)
  fromPoint = uncurry Speed

instance R.Variate Speed where
  uniform g = do
    vX <- R.uniform g
    vY <- R.uniform g
    return Speed{..}

  uniformR (Speed x y, Speed x' y') g = do
    vX <- R.uniformR (x, x') g
    vY <- R.uniformR (y, y') g
    return Speed{..}

data Asteroid where
  Circle :: { cPos :: !Pos
            , cRad :: Float -- ^ Mass of the object / planet
            , cVel :: Speed
            , cSho :: !Bool -- ^ Hover over position
            , cTra :: !Bool -- ^ Track position
            , cAcc :: Acc -- ^ Acceleration of the asteroid
            , cPat :: [G.Point] -- ^ last N points in our path
            , cMas :: !Float -- ^ mass
            } -> Asteroid
  Group :: { gPos :: !Pos
            , gRad :: !Float -- ^ Mass of the object / planet
            , gVel :: Speed
            , gSho :: !Bool -- ^ Hover over position
            , gTra :: !Bool -- ^ Track position
            , gAcc :: Acc -- ^ Acceleration of the asteroid
            , gPat :: [G.Point] -- ^ last N points in our path
            , gElems :: [Asteroid]
            , gMas :: Float -- ^ mass
           } -> Asteroid
  deriving (Show, Eq)

instance Object Asteroid where
  position (Circle{cPos, ..}) = toPoint cPos
  position (Group{gPos, ..}) = toPoint gPos
#if 0
  setPos p@(x, y) (c@Circle{}) = c{ cPos = Pos x y }
  setPos p@(x, y) (g@Group{}) = g{ gPos = Pos x y
                                 , gElems = do
                                     (deltaPos, e) <- gElems g
                                     return (p G.- (position g G.+ deltaPos), setPos p g)
                                 }
#endif
  radius (Circle{cRad, ..}) = cRad
  radius (Group{gRad, ..}) = gRad

  mass (Circle{cMas, ..}) = cMas
  mass (Group{gMas, ..}) = gMas

  velocity (Circle{cVel, ..}) = toPoint cVel
  velocity (Group{gVel, ..}) = toPoint gVel

  acceleration (Circle{cAcc, ..}) = toPoint cAcc
  acceleration (Group{gAcc, ..}) = toPoint gAcc

instance R.Variate Asteroid where
  uniform g = do
    let
      circleDef :: forall m . (PrimMonad m) => (R.Gen (PrimState m)) -> m Asteroid
      circleDef g = do
        cPos <- R.uniform g
        cRad <- R.uniform g
        cVel <- R.uniform g
        cAcc <- R.uniform g
        cMas <- R.uniform g
        let
          cSho = False
          cTra = False
          cPat = []
        return Circle{..}

    R.uniformR (0, 1) g >>= \case
      (0 :: Int) -> circleDef g
      _ -> circleDef g

  uniformR (Circle p@(Pos x y) r v _ _ a _ m, Circle p'@(Pos x' y') r' v' _ _ a' _ m') g = do
    cPos <- R.uniformR (p, p') g
    cRad <- R.uniformR (r, r') g
    cVel <- R.uniformR (v, v') g
    cAcc <- R.uniformR (a, a') g
    cMas <- R.uniformR (m, m') g
    let
      cSho = False
      cTra = False
      cPat = []
    return Circle{..}

data Model where
  Model :: { modCount :: Int
           , modCircles :: ![Asteroid]
           } -> Model
  deriving (Show)

instance R.Variate Model where
  uniform g = do
    modCount <- R.uniformR (1, 100) g
    modCircles <- replicateM modCount (R.uniform g)
    return Model{..}

  uniformR (Model c cxs, Model c' cxs') g = do
    modCount <- R.uniformR (c, c') g
    modCircles <- replicateM modCount genElem
    return Model{..}
      where
        -- genElem :: forall m . (PrimMonad m) => m Asteroid
        genElem = do
          let
            cs = cxs ++ cxs'
            x@(minX, maxX) = (minimumBy (compare)) &&& (maximumBy (compare)) $ (pX . cPos) <$> cs
            y@(minY, maxY) = (minimumBy (compare)) &&& (maximumBy (compare)) $ (pY . cPos) <$> cs
            r@(minRad, maxRad) = (minimumBy (compare)) &&& (maximumBy (compare)) $ cRad <$> cs
            vx@(minVelX, maxVelX) = (minimumBy (compare)) &&& (maximumBy (compare)) $ (vX . cVel) <$> cs
            vy@(minVelY, maxVelY) = (minimumBy (compare)) &&& (maximumBy (compare)) $ (vY . cVel) <$> cs
            ax@(minAccX, maxAccX) = (minimumBy (compare)) &&& (maximumBy (compare)) $ (aX . cAcc) <$> cs
            ay@(minAccY, maxAccY) = (minimumBy (compare)) &&& (maximumBy (compare)) $ (aY . cAcc) <$> cs
            m@(minMass, maxMass) = (minimumBy compare) &&& (maximumBy compare) $ mass <$> cs

          px <- R.uniformR x g
          py <- R.uniformR y g
          r  <- R.uniformR r g
          vx <- R.uniformR vx g
          vy <- R.uniformR vy g
          ax <- R.uniformR ax g
          ay <- R.uniformR ay g
          m  <- R.uniformR m g
          return $ Circle (Pos px py) r (Speed vx vy) False True (Acc 0 0) [] m

someFunc :: IO ()
someFunc = do
  putStrLn "someFunc.1"
  rng <- R.createSystemRandom
  let
    wF = fromIntegral w / 2
    hF = fromIntegral h / 2
  initModel <- R.uniformR ( Model 20 [Circle (Pos (-wF) (-hF)) 4 (Speed (- 10) (- 10)) False False (Acc (- 10) (- 10)) [] 10]
                          , Model 80 [Circle (Pos (wF) (hF)) 8 (Speed 10 10) False False (Acc 10 10) [] 80])
               rng
  print initModel
  G.playIO
    window
    background
    60
    (initModel)
    (\m -> do
       let
         modPic, frontierPic :: [G.Picture]
         modPic = do
           c <- modCircles m
           return $ renderAsteroid c

         frontierPic = do
           return $ G.rectangleWire (fromIntegral w) (fromIntegral h)

       (return . G.pictures) (modPic <> frontierPic)
    ) -- ^ render model
    (\e m -> do
        case e of
          G.EventKey (G.Char 'q') _ _ _ -> die "user pressed `q`"
          G.EventKey (G.MouseButton G.LeftButton) G.Down _ p@(x,y) -> return m{ modCircles = fmap (tracWithPos p) (modCircles m)}
          G.EventMotion p@(x, y) -> return m{ modCircles = fmap (showWithPos p) (modCircles m)}
          _ -> do
            print e
            return m) -- ^ event processor
    step -- ^ iteration step
  -- G.display window background drawing
  putStrLn "someFunc.2"
    where
      dist :: (Float, Float) -> (Float, Float) -> Float
      dist (x,y) (x',y') = sqrt $ sq (x-x') + sq (y-y')

      sq :: Float -> Float
      sq = (^^ 2)

      tracWithPos :: (Float, Float) -> Asteroid -> Asteroid
      tracWithPos p@(x, y) (Circle pos@(Pos px py) sz speed shw tra acc pth m) = Circle pos sz speed shw nTra acc pth m
        where
          nTra :: Bool
          nTra
            | dist p (px, py) < sz = not tra
            | otherwise = tra

      tracWithPos p@(x, y) (Group pos@(Pos px py) sz speed shw tra acc pth elems m) = Group pos sz speed shw nTra acc pth elems m
        where
          nTra :: Bool
          nTra
            | dist p (px, py) < sz = not tra
            | otherwise = tra

      showWithPos :: (Float, Float) -> Asteroid -> Asteroid
      showWithPos p@(x, y) (Circle pos@(Pos px py) sz speed _ tra acc pth m) = Circle pos sz speed shw tra acc pth m
        where
          shw :: Bool
          shw | dist p (px, py) < sz = True
              | otherwise = False

      showWithPos p@(x, y) (Group pos@(Pos px py) sz speed _ tra acc pth elems m) = Group pos sz speed shw tra acc pth elems m
        where
          shw :: Bool
          shw | dist p (px, py) < sz = True
              | otherwise = False

      renderAsteroid :: Asteroid -> G.Picture
      renderAsteroid c@Circle{..}
        | cSho == False && cTra == False = G.translate (pX cPos) (pY cPos) (aCirc G.black)
        | otherwise = trails G.red <> G.translate (pX cPos) (pY cPos) ( G.pictures [ aCirc G.red
                                                                                   , G.color G.red $ G.scale 0.05 0.05 . G.text . show $ (position c)
                                                                                   , G.color G.red $ G.translate 0 (- 8) . G.scale 0.05 0.05 . G.text . show $ (velocity c)
                                                                                   , G.color G.red $ G.translate 0 (- 16) . G.scale 0.05 0.05 . G.text . show $ (acceleration c)
                                                                                   ])
          where
            trails :: G.Color -> G.Picture
            trails color = G.color color $ G.line cPat

            aCirc :: G.Color -> G.Picture
            aCirc color = G.pictures [ G.color color (G.thickCircle 2 cRad)
                                     , G.color (G.dim color) speedVec
                                     , G.color (G.bright G.blue) accelVec
                                     ]
            speedVec :: G.Picture
            speedVec = G.line $ fmap ((G.+) radV) [ (0,0), vel ]
              where
                vel = velocity c
                radV = radius c G.* G.normalizeV vel

            accelVec :: G.Picture
            accelVec = G.line $ fmap ((G.+) radV) [(0,0), acc]
              where
                acc = acceleration c
                radV = radius c G.* G.normalizeV acc

      renderAsteroid g@Group{..}
        | gSho == False && gTra == False = G.translate (pX gPos) (pY gPos) (aCirc G.green)
        | otherwise = trails G.blue <> G.translate (pX gPos) (pY gPos) ( G.pictures [ aCirc G.blue
                                                                                   , G.color G.blue $ G.scale 0.05 0.05 . G.text . show $ (position g)
                                                                                   , G.color G.blue $ G.translate 0 (- 8) . G.scale 0.05 0.05 . G.text . show $ (velocity g)
                                                                                   , G.color G.blue $ G.translate 0 (- 16) . G.scale 0.05 0.05 . G.text . show $ (acceleration g)
                                                                                   ])
          where
            trails :: G.Color -> G.Picture
            trails color = G.color color $ G.line gPat

            aCirc :: G.Color -> G.Picture
            aCirc color = G.pictures $ [ G.color color (G.circle gRad)
                                       , G.color (G.dim color) speedVec
                                       , G.color (G.bright G.blue) accelVec
                                       ] <> fmap (\e -> G.translate (x $ position e) (y $ position e) $ G.color color (G.circle (radius e))) gElems

            speedVec :: G.Picture
            speedVec = G.line $ fmap ((G.+) radV) [ (0,0), vel ]
              where
                vel = velocity g
                radV = radius g G.* G.normalizeV vel

            accelVec :: G.Picture
            accelVec = G.line $ fmap ((G.+) radV) [(0,0), acc]
              where
                acc = acceleration g
                radV = radius g G.* G.normalizeV acc

      step :: Float -> Model -> IO Model
      step time (Model modCount cs) = do
        let
          m = Model{..}
        -- print m
        return m
        where
          modCircles = fuseNearby modCircles' []
            where
              fuseNearby :: [Asteroid] -> [Asteroid] -> [Asteroid]
              fuseNearby [] nels = nels
              fuseNearby (e:els) nels = fuseNearby (filter (farFrom e) els) (foldr fuse e (filter (closeTo e) els):nels)

              farFrom :: Asteroid -> Asteroid -> Bool
              farFrom e1 e2 = not $ closeTo e1 e2

              closeTo :: Asteroid -> Asteroid -> Bool
              closeTo c oc = G.magV (position c G.- position oc) < radius c + radius oc

              fuse :: Asteroid -> Asteroid -> Asteroid
              fuse c@(Circle p@(Pos px py) r s@(Speed vx vy) shw tra a@(Acc ax ay) pth m) oc@(Circle op@(Pos opx opy) or os@(Speed ovx ovy) oshw otra oa@(Acc oax oay) opth om) = Group{..}
                where
                  gPosToPoint = 0.5 G.* (position c G.+ position oc)
                  gPos = fromPoint gPosToPoint

                  gRad = G.magV $ maximumBy (on compare G.magV) [ env c
                                                                , env oc
                                                                ]
                         where
                           env c =
                             let
                               deltaV = position c G.- gPosToPoint
                               uniV = G.normalizeV deltaV
                             in
                               deltaV G.+ (radius c G.* uniV)

                  gVel = fromPoint . ((/ gRad) *** (/ gRad)) $ mass c G.* velocity c G.+ mass oc G.* velocity oc
                  gSho = any (== True) [shw, oshw]
                  gTra = any (== True) [tra, otra]
                  gAcc = fromPoint . ((/ gRad) *** (/ gRad)) $ mass c G.* acceleration c G.+ mass oc G.* acceleration oc
                  gPat = pth
                  gElems = [ c{ cPos = fromPoint $ position c G.- gPosToPoint
                              , cAcc = gAcc
                              , cVel = gVel
                              }
                           , oc{ cPos = fromPoint $ position oc G.- gPosToPoint
                               , cAcc = gAcc
                               , cVel = gVel
                               }
                           ]
                  gMas = m + om

              fuse c@(Circle p@(Pos px py) r s@(Speed vx vy) shw tra a@(Acc ax ay) pth m) g@(Group gp@(Pos gpx gpy) gr gs@(Speed gvx gvy) gshw gtra ga@(Acc gax gay) gpth gelems gm) = Group{..}
                where
                  positions = position c : fmap (\e -> position g G.+ position e) gelems
                  (count, ps@(psX, psY)) = foldr (\(x, y) (count, (tx, ty)) -> (count+1, (x+tx, y+ty))) (0, (0,0)) positions
                  gPosToPoint = ((/ (fromIntegral count)) *** (/ (fromIntegral count))) ps
                  gPos = fromPoint gPosToPoint

                  envelopVs = zipWith
                       (\e p ->
                           let
                             deltaV = p G.- gPosToPoint
                             uniV = G.normalizeV deltaV
                           in
                             (deltaV G.+ (radius e G.* uniV)))
                       (c:gelems)
                       positions
                  gRad = G.magV $ maximumBy (on compare G.magV) envelopVs

                  elemsVel = velocity g
                  movment = foldr (\(e, m, v) movment -> (movment G.+ (m G.* v))) (0,0) ((c, mass c, velocity c):fmap (\ge -> (ge, mass ge, elemsVel)) gelems)
                  gVel = fromPoint . ((/ gMas) *** (/ gMas)) $ movment

                  gSho = any (== True) [shw, gshw]
                  gTra = any (== True) [tra, gtra]

                  force = foldr (\(e, m, a) force -> force G.+ (m G.* a)) (0,0) ((c, mass c, acceleration c):fmap (\ge -> (ge, mass ge, acceleration g)) gelems)
                  gAcc = fromPoint . ((/ gMas) *** (/ gMas)) $ force
                  gPat = gpth
                  gElems = c{ cPos = fromPoint $ position c G.- gPosToPoint
                            , cAcc = gAcc
                            , cVel = gVel
                            } : (fmap (\case
                                          e@Circle{} -> e{ cPos = fromPoint $ position g G.+ position e G.- gPosToPoint
                                                         , cAcc = gAcc
                                                         , cVel = gVel
                                                         }
                                          g@Group{} -> error "unexpected nested Group!"
                                      ) gelems)
                  gMas = m + gm

              fuse g@(Group{..}) c@(Circle{..}) = fuse c g

              fuse a@Group{} b@Group{} = foldr merge b elems
                where
                  merge :: Asteroid -> Asteroid -> Asteroid
                  merge c@Circle{} g@Group{} = fuse c g
                  merge _ _ = error "unexpected type of merge !"

                  elems = fmap (\case
                                   c@Circle{} -> c{ cPos = fromPoint $ position c G.+ position a }
                                   g@Group{} -> error "unexpected nested Group!") (gElems a)

          modCircles' = stepAsteroid <$> cs

          k = 50 -- ^ Multiplying Konstant

          stepAsteroid :: Asteroid -> Asteroid
          stepAsteroid g@(Group gp@(Pos gpx gpy) gr (Speed gvx gvy) gshw gtra (Acc gax gay) gpth gelems gm) = Group{..}
            where
              v@(vx, vy) = velocity g G.+ time G.* acceleration g
              p'@(px', py') = position g G.+ time G.* v
              ((px, vx'), (py, vy')) = (if abs px' + radius g > wF then (if px' < 0 then px' + (abs px' + radius g - wF) else px' - (abs px' + radius g - wF), - vx) else (px', vx), if abs py' + radius g > hF then (if py' < 0 then py' + (abs py' + radius g - hF) else py' - (abs py' + radius g - hF), - vy) else (py', vy))

              p = (px, py)
              gPos = fromPoint p

              gRad = gr

              v' = (vx', vy')
              gVel = fromPoint v'

              gSho = gshw
              gTra = gtra

              nearEs = take (max 5 (ceiling $ fromIntegral (length cs - length gelems) / 2.0)) $ sortBy (nearF) cs
              nearF :: Asteroid -> Asteroid -> Ordering
              nearF a b = compare ca cb
                where
                  pg = position g
                  ca = G.magV $ position a G.- pg
                  cb = G.magV $ position b G.- pg

              a'@(ax', ay') = (/ (mass g)) *** (/ (mass g)) $ foldr1 (G.+) (fmap force nearEs)

              -- | force vector
              force :: Asteroid -> G.Vector
              force og@Group{}
                | collide = (0, 0)
                | otherwise = magV G.* unitV
                where
                  distV = position og G.- position g
                  unitV = G.normalizeV distV
                  magV = k * mass og * mass g / (sq $ G.magV distV)
                  collide = G.magV distV < (radius og + radius g)

              force c@(Circle op@(Pos opx opy) or _ _ _ _ _ _)
                | collide = (0, 0) -- no force between an object and itself
                | otherwise = magV G.* unitV
                where
                  distV = position c G.- position g
                  unitV = G.normalizeV distV
                  magV = k * mass c * mass g / (sq $ G.magV distV)
                  collide = G.magV distV < (radius g + radius c)

              gAcc = fromPoint a'

              gPat
                | not gtra = []
                | otherwise = take 720 $ position g : gpth

              gElems = fmap (\case
                                c@Circle{} -> c{ cAcc = gAcc
                                               , cVel = gVel
                                               }
                                g@Group{} -> error "unexpected nested Group")
                       gelems

              gMas = gm

          stepAsteroid c@(Circle p@(Pos px py) r (Speed vx vy) shw tra (Acc ax ay) pth m) = Circle (Pos px' py') r (Speed vx' vy') shw tra (Acc ax' ay') pth' m
            where
              vx'' = vx + ax * time
              vy'' = vy + ay * time

              px'' = px + vx'' * time
              py'' = py + vy'' * time

              ((px', vx'), (py', vy')) = (if abs px'' + radius c >= wF then (if px'' < 0 then px'' + (abs px'' + radius c - wF) else px'' - (abs px'' + radius c - wF), - vx'') else (px'', vx''), if abs py'' + radius c >= hF then (if py'' < 0 then py'' + (abs py'' + radius c - hF) else py'' - (abs py'' + radius c - hF), - vy'') else (py'', vy''))

              nearEs = take (max 5 (ceiling $ fromIntegral (length cs) / 2.0)) . drop 1 $ sortBy (nearF) cs
              nearF :: Asteroid -> Asteroid -> Ordering
              nearF a b = compare ca cb
                where
                  pc = position c
                  ca = G.magV $ position a G.- pc
                  cb = G.magV $ position b G.- pc

              (ax', ay') = (/ (mass c)) *** (/ (mass c)) $ foldr1 (G.+) (fmap force nearEs)

              -- | force vector
              force :: Asteroid -> G.Vector
              force og@Group{}
                | collide = (0, 0)
                | otherwise = magV G.* unitV
                where
                  distV = position og G.- position c
                  unitV = G.normalizeV distV
                  magV = k * mass og * mass c / (sq $ G.magV distV)
                  collide = G.magV distV < (radius og + radius c)

              force oc@(Circle op@(Pos opx opy) or _ _ _ _ _ _)
                | collide = (0, 0) -- no force between an object and itself
                | otherwise = magV G.* unitV
                where
                  distV = position oc G.- position c
                  unitV = G.normalizeV distV
                  magV = k * mass oc * mass c / (sq $ G.magV distV)
                  collide = G.magV distV < (radius oc + radius c)

              pth'
                | not tra = []
                | otherwise = take 720 $ position c : pth

      window :: G.Display
      window = G.InWindow "Nice Window" (w, h) (ox, oy)

      ox, oy, w, h :: Int
      ox = 0
      oy = 0
      w = 800
      h = 800

      wF = fromIntegral w / 2
      hF = fromIntegral h / 2

      background :: G.Color
      background = G.white
